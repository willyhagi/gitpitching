@snap[midpoint]
## A Note on Chaos Theory
@snapend

@snap[south-west]
Author: Willy Hagi
@snapend

---
The Begginings

@snap[west span - 45]
@ul
* A mathematician and meteorologist from the MIT
* Father of Chaos Theory
* Discovered the Butterfly Effect using an LGP-30 desk computer
* Also liked hiking and climbing 
@ulend 
@snapend

@snap[east span -60]
![Edward Lorenz](assets/img/edward_lorenz.jpg)&position=right&size=75%
@snapend


+++?color=black

@snap[west span-45]
# Step by Step
@snapend

@snap[north-east span-60]
@box[bg-purple text-white](Step 1.#Lorem ipsum dolor sit amet, consectetur adipiscing elit.)
@snapend

@snap[east span-60]
@box[bg-orange text-white](Step 2.#Sed do eiusmod tempor incididunt ut labore ut enim ad.)
@snapend

@snap[south-east span-60]
@box[bg-pink text-white](Step 3.#Cupidatat non proident sunt in culpa officia veniam quis.)
@snapend